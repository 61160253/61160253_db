/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.store_db.POC;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author NOOM HR
 */
public class TestInsertProduct {

    public static void main(String[] args) {
        Connection conn = null;
        String dbPath = "./DB/STORE.DB";
        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:" + dbPath);
            System.out.print("Database connection");
        } catch (ClassNotFoundException ex) {
            System.out.println("Error: JDBC is not exist");
        } catch (SQLException ex) {
            System.out.println("Error: Database cannot connection");
        }

        try {
            String sql = "INSERT INTO PRODUCT (NAME,PRICE) VALUES (?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, "O LEING");
            stmt.setDouble(2,20);
            int row = stmt.executeUpdate();
            System.out.println(" After row " + row);
            int id = 1;
            ResultSet result = stmt.getGeneratedKeys();
            if(result.next()){
                id = result.getInt(1);
            }
            System.out.println(" After row " + row + " id: " + id);
            
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            if (conn != null) {
                conn.close();
            }
            conn.close();
        } catch (SQLException ex) {
            System.out.println("Error: Cannot close database");
        }
    }

}
