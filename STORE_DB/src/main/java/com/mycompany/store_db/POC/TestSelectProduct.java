/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.store_db.POC;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author NOOM HR
 */
public class TestSelectProduct {

    public static void main(String[] args) {
        Connection conn = null;
        String dbPath = "./DB/STORE.DB";
        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:" + dbPath);
            System.out.print("Database connection");
        } catch (ClassNotFoundException ex) {
            System.out.println("Error: JDBC is not exist");
        } catch (SQLException ex) {
            System.out.println("Error: Database cannot connection");
        }
        
        try {
            String sql = "SELECT ID, NAME, PRICE FROM PRODUCT";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()){
                int id = result.getInt("id");
                String name = result.getString("name");
                double price = result.getDouble("price");
                System.out.println(id + " " + name + " " + price);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            if (conn != null) {
                conn.close();
            }
            conn.close();
        } catch (SQLException ex) {
            System.out.println("Error: Cannot close database");
        }
    }
}
