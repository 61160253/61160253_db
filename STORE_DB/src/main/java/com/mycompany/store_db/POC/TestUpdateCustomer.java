/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.store_db.POC;

import Database.Database;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author NOOM HR
 */
public class TestUpdateCustomer {
    public static void main(String[] args) {
Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "UPDATE CUSTOMER SET NAME = ?,TEL = ? WHERE ID = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1,"DAO 1");
            stmt.setString(2,"0009998877");
            stmt.setInt(3,6);
            int row = stmt.executeUpdate();
           
            System.out.println("Affect row "+ row );
            
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectUser.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
    }
}

